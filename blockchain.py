import functools
import pickle

from block import Block
from transaction import Transaction
from utility.hash_util import hash_block
from utility.verification import Verification
from wallet import Wallet

reward_mining = 10


class Blockchain:
    def __init__(self, hosting_node):
        self.__chain = []
        self.__open_transactions = []
        self.load_data()
        self.hosting_node = hosting_node

    def get_chain(self):
        return self.__chain

    def get_open_transactions(self):
        return self.__open_transactions

    def load_data(self):
        try:
            with open('blockchain.p', mode='rb') as f:
                file_content = pickle.loads(f.read())
                self.__chain = file_content['chain']
                self.__open_transactions = file_content['open']
        except (IOError, IndexError):
            print('File not found!')
            self.init_data()

    def init_data(self):
        block = Block(index=0, previous_hash='', transactions=[], proof=100, timestamp=0)
        self.__chain = [block]
        self.__open_transactions = []

    def save_data(self):
        try:
            with open('blockchain.p', mode='wb') as f:
                formatted_save_data = {
                    'chain': self.__chain,
                    'open': self.__open_transactions
                }
                f.write(pickle.dumps(formatted_save_data))
        except IOError:
            print('Failed saving data!')

    def proof_of_work(self):
        last_block = self.__chain[-1]
        last_hashed = hash_block(last_block)

        proof_number = 0
        while not Verification.valid_proof_work(self.__open_transactions, last_hashed, proof_number):
            proof_number += 1
        return proof_number

    def get_balance(self):
        if self.hosting_node is None:
            return None
        participant = self.hosting_node
        tx_sender = [[tx.amount for tx in block.transactions if tx.sender == participant] for block in self.__chain]
        tx_open_sender = [tx.amount for tx in self.__open_transactions if tx.sender == participant]
        tx_sender.append(tx_open_sender)

        amount_sent = functools.reduce(lambda tx_sum, tx_am: tx_sum + (sum(tx_am) if len(tx_am) > 0 else 0), tx_sender,
                                       0)

        tx_recipient = [[tx.amount for tx in block.transactions if tx.recipient == participant] for block in
                        self.__chain]
        amount_received = functools.reduce(lambda tx_sum, tx_am: tx_sum + (sum(tx_am) if len(tx_am) > 0 else 0),
                                           tx_recipient,
                                           0)
        return amount_received - amount_sent

    def get_last_blockchain_value(self):
        """Return the last value of the bockchain. None is no value existing"""
        if len(self.__chain) < 1:
            return None
        return self.__chain[-1]

    def add_transaction(self, recipient, sender, signature, amount=1.0):
        """Add a new transaction to the blockchain.
        """
        if self.hosting_node is None:
            return False

        transaction = Transaction(
            sender=sender,
            recipient=recipient,
            amount=amount,
            signature=signature)
        if Verification.verify_transaction(transaction, self.get_balance):
            self.__open_transactions.append(transaction)
            self.save_data()
            return True
        return False

    def mine_block(self):
        if self.hosting_node is None:
            return None
        last_block = self.__chain[-1]
        hashed_block = hash_block(last_block)
        proof = self.proof_of_work()

        reward_transaction = Transaction(
            sender='MINING',
            recipient=self.hosting_node,
            amount=reward_mining,
            signature='')
        copied_transactions = self.__open_transactions[:]
        for tx in copied_transactions:
            if not Wallet.verify_transaction(tx):
                return None
        copied_transactions.append(reward_transaction)

        block = Block(
            index=len(self.__chain),
            previous_hash=hashed_block,
            transactions=copied_transactions,
            proof=proof)

        self.__chain.append(block)
        self.__open_transactions = []
        self.save_data()
        return block
