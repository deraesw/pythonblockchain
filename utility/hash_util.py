"""Provides methods to hash data"""


import hashlib
import json


def hash_string_256(string):
    """Hashed a given string to sha256"""
    return hashlib.sha256(string).hexdigest()


def hash_block(block):
    """Hashed the content of a block after we transform into a directory"""
    hashable_block = block.__dict__.copy()
    hashable_block['transactions'] = [tx.to_ordered_dict() for tx in hashable_block['transactions']]
    return hash_string_256(json.dumps(hashable_block, sort_keys=True).encode())
