
class Converter:

    @staticmethod
    def convert_chain_to_dict(chain):
        chain_dict = [block.__dict__.copy() for block in chain]
        for block in chain_dict:
            block['transactions'] = [tx.__dict__ for tx in block['transactions']]
        return chain_dict

    @staticmethod
    def convert_block_to_dict(block):
        block_dict = block.__dict__.copy()
        block_dict['transactions'] = [tx.__dict__ for tx in block_dict['transactions']]
        return block_dict
