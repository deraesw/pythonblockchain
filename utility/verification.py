"""Provides verification methods"""


from utility.hash_util import hash_string_256, hash_block
from wallet import Wallet


class Verification:

    @staticmethod
    def valid_proof_work(transactions, previous_hash, proof_number):
        guess = (str([tx.to_ordered_dict() for tx in transactions]) + str(previous_hash) + str(proof_number)).encode()
        guess_hashed = hash_string_256(guess)
        return guess_hashed[0:2] == '00'

    @classmethod
    def verify_blockchain(cls, blockchain):
        for (index, block) in enumerate(blockchain):
            if index == 0:
                continue
            if block.previous_hash != hash_block(blockchain[index - 1]):
                return False
            if not cls.valid_proof_work(block.transactions[:-1], block.previous_hash, block.proof):
                print('Proof of work is invalid')
                return False
        return True

    @staticmethod
    def verify_transaction(transaction, get_balance, check_funds = True):
        if check_funds:
            balance = get_balance()
            return balance >= transaction.amount and Wallet.verify_transaction(transaction)
        else:
            return Wallet.verify_transaction(transaction)

    @classmethod
    def verify_transactions(cls, open_transactions, get_balance):
        return all([cls.verify_transaction(tx, get_balance, False) for tx in open_transactions])
