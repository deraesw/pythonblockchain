from flask import Flask, jsonify, request, send_from_directory
from flask_cors import CORS

from wallet import Wallet
from blockchain import Blockchain
from utility.converter import Converter

app = Flask(__name__)
wallet = Wallet()
blockchain = Blockchain(wallet.public_key)
CORS(app)


@app.route('/', methods=['GET'])
def home_test():
    return send_from_directory('ui', 'node.html')


@app.route('/wallet', methods=['POST'])
def create_wallet():
    wallet.create_keys()
    if wallet.save_keys():
        blockchain.hosting_node = wallet.public_key
        balance = blockchain.get_balance()
        response = {
            'public_key': wallet.public_key,
            'private_key': wallet.private_key,
            'funds': balance
        }
        return jsonify(response), 201
    else:
        response = {
            'message': 'Saving the keys is failed.'
        }
        return jsonify(response), 500


@app.route('/wallet', methods=['GET'])
def load_wallet():
    if wallet.load_keys():
        blockchain.hosting_node = wallet.public_key
        balance = blockchain.get_balance()
        response = {
            'public_key': wallet.public_key,
            'private_key': wallet.private_key,
            'funds': balance
        }
        return jsonify(response), 201
    else:
        response = {
            'message': 'Loading the keys is failed.'
        }
        return jsonify(response), 500


@app.route('/balance', methods=['GET'])
def get_balance():
    balance = blockchain.get_balance()
    if balance is not None:
        response = {
            'funds': balance
        }
        return jsonify(response), 201
    else:
        response = {
            'message': 'Loading balance.',
            'wallet_status': wallet.public_key is not None
        }
        return jsonify(response), 500


@app.route('/transaction', methods=['POST'])
def add_transaction():
    if wallet.public_key is None:
        response = {
            'message': 'Not wallet found!'
        }
        return jsonify(response), 500
    values = request.get_json()
    if not values:
        response = {
            'message': 'Not values found!'
        }
        return jsonify(response), 400
    required_fields = ['recipient', 'amount']
    if not all(req_field in values for req_field in required_fields):
        response = {
            'message': 'Missing values!'
        }
        return jsonify(response), 400
    signature = wallet.sign_transaction(wallet.public_key, values['recipient'], values['amount'])
    success = blockchain.add_transaction(values['recipient'], wallet.public_key, signature, values['amount'])
    if success:
        response = {
            'message': 'Add transaction',
            'transaction': {
                'sender': wallet.public_key,
                'recipient': values['recipient'],
                'signature': signature,
                'amount': values['amount']
            },
            'funds': blockchain.get_balance()
        }
        return jsonify(response), 201
    else:
        response = {
            'message': 'Could not add transaction!'
        }
        return jsonify(response), 400


@app.route('/mine', methods=['POST'])
def mine():
    block = blockchain.mine_block()
    if block is not None:
        block_dict = Converter.convert_block_to_dict(block)
        response = {
            'message': 'Block successfully mined',
            'block': block_dict,
            'funds': blockchain.get_balance()
        }
        return jsonify(response), 201
    else:
        response = {
            'message': 'Error occurred. Could not mine a block',
            'wallet_status': wallet.public_key is not None
        }
        return jsonify(response), 500


@app.route('/transactions', methods=['GET'])
def get_open_transactions():
    transactions = blockchain.get_open_transactions()
    transaction_dict = [tx.__dict__ for tx in transactions]
    return jsonify(transaction_dict), 200


@app.route('/chain', methods=['GET'])
def get_chain():
    chain = blockchain.get_chain().copy()
    chain_dict = Converter.convert_chain_to_dict(chain)
    return jsonify(chain_dict), 200


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
