from blockchain import Blockchain
from utility.verification import Verification
from wallet import Wallet


class Node:

    def __init__(self):
        # self.wallet.public_key = str(uuid4())
        self.wallet = Wallet()
        self.blockchain = Blockchain(self.wallet.public_key)

    def enter_new_transaction(self):
        tx_recipient = input('Please enter the recipient:')
        tx_amount = float(input('Please enter the amount: '))
        return tx_recipient, tx_amount

    def get_user_choice(self):
        return input('Choose: ')

    def displaying_all_transaction(self):
        for block in self.blockchain.get_chain():
            print('Output block')
            print(block)

    def print_main_menu(self):
        print('Please choose')
        print('1: Enter a new transaction')
        print('2: Mine a new block')
        print('3: Output blockhain contain')
        print('4: Checks open transactions')
        print('5: Create a wallet')
        print('6: Load a wallet')
        print('7: Save wallet')
        print('q: Quit')

    def user_input(self):
        while True:
            self.print_main_menu()
            user_choice = self.get_user_choice()
            if user_choice == '1':
                if self.wallet.public_key is None:
                    print('Please create or load a wallet')
                else:
                    tx_data = self.enter_new_transaction()
                    recipient, amount = tx_data
                    signature = self.wallet.sign_transaction(self.wallet.public_key, recipient, amount)
                    result = self.blockchain.add_transaction(
                        recipient,
                        amount=amount,
                        sender=self.wallet.public_key,
                        signature=signature)
                    if not result:
                        print('Failed to add new transaction')
            elif user_choice == '2':
                if not self.blockchain.mine_block():
                    if self.wallet.public_key is None:
                        print('Failed to mine a block. Do you have Wallet ?')
                    else:
                        print('Failed to mine a block.')
            elif user_choice == '3':
                self.displaying_all_transaction()
            elif user_choice == '4':
                if Verification.verify_transactions(
                        self.blockchain.get_open_transactions(),
                        self.blockchain.get_balance):
                    print("All open transaction are valid")
                else:
                    print("There are some invalid transaction")
            elif user_choice == '5':
                self.wallet.create_keys()
                self.blockchain.hosting_node = self.wallet.public_key
            elif user_choice == '6':
                self.wallet.load_keys()
                self.blockchain.hosting_node = self.wallet.public_key
            elif user_choice == '7':
                self.wallet.save_keys()
            elif user_choice == 'q':
                break
            else:
                print('Unknown option, please select one of the available options!')
            if not Verification.verify_blockchain(self.blockchain.get_chain()):
                break
            print('Balance of {} : {:6.2f}'.format(self.wallet.public_key, self.blockchain.get_balance()))

        print('End')


if __name__ == '__main__':
    node = Node()
    node.user_input()
